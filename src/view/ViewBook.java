package view;

import controller.BookControl;
import java.util.List;
import javax.swing.JOptionPane;
import model.Book;

/**
 *
 * @author Tiago Martins Vargas
 *
 */
public class ViewBook {

    public static void main(String[] args) {
        String option;
        BookControl bC = new BookControl();
        List<Book> books;
        Book book;
        do {
            option = JOptionPane.showInputDialog("Digite a opção desejada:"
                    + "\n1) Cadastrar Livro:\n2) Pesquisar por Autor:\n3) Pesquisar por Título:\n4) Excluir:\n5) Listar Todos:\n6) Sair:");
            switch (option) {
                case "1":
                    book = registerBook();
                    bC.save(book);
                    break;
                case "2":
                    String author = JOptionPane.showInputDialog("Pesquise pelo nome do autor");
                    books = bC.searchByAuthor(author);
                    if (books.isEmpty()) {
                        JOptionPane.showMessageDialog(null, "Nenhum livro encontrado!");
                    } else {
                        listBook(books);
                    }
                    break;
                case "3":
                    String title = JOptionPane.showInputDialog("Pesquise pelo título do livro");
                    books = bC.searchByTitle(title);
                    if (books.isEmpty()) {
                        JOptionPane.showMessageDialog(null, "Nenhum livro encontrado!");
                    } else {
                        listBook(books);
                    }
                    break;
                case "4":
                    books = bC.getBooks();
                    listBook(books);
                    String code = JOptionPane.showInputDialog("Digite o codigo do livro para excluí-lo");
                    bC.delete(code);
                    break;
                case "5":
                    books = bC.getBooks();
                    listBook(books);
                    break;
                case "6":
                    System.exit(0);
                    break;
            }
        } while (!"6".equals(option));
    }

    private static Book registerBook() {
        Book book = new Book();
        book.setTitle(JOptionPane.showInputDialog("Insira o Titulo do Livro:"));
        book.setPublisher(JOptionPane.showInputDialog("Insira o Editora do Livro:"));
        book.setAuthor(JOptionPane.showInputDialog("Insira o Autor do Livro:"));
        book.setYear(JOptionPane.showInputDialog("Insira o Ano do Livro:"));
        book.setDescription(JOptionPane.showInputDialog("Insira a Descrição do Livro:"));
        return book;
    }
    
        private static void listBook(List<Book> books) {

        for (Book book : books) {
            System.out.println("Código: " + book.getCode());
            System.out.println("Titulo: " + book.getTitle());
            System.out.println("Autor: " + book.getAuthor());
            System.out.println("Editora: " + book.getPublisher());
            System.out.println("Descrição: " + book.getDescription());
            System.out.println("");
        }
        System.out.println("");
    }

}
