package controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.swing.JOptionPane;
import model.Book;

/**
 *
 * @author Tiago Martins Vargas
 *
 */
public class BookControl {

    private List<Book> books = new ArrayList<>();

    private String generateCode() {
        String code;
        Calendar cal = Calendar.getInstance();
        int day = cal.get(Calendar.DAY_OF_MONTH);

        if (books.isEmpty()) {
            code = "1000" + "-" + day;
        } else {
            Book book = books.get(books.size() - 1);
            code = book.getCode().substring(0, 4);
            int bookNumber = Integer.parseInt(code) + 1;
            code = Integer.toString(bookNumber) + "-" + day;
        }
        return code;
    }

    public void save(Book book) {
        book.setCode(generateCode());
        books.add(book);
        JOptionPane.showMessageDialog(null, "Livro " + book.getTitle() + " Salvo com sucesso!");
    }

    public List<Book> getBooks() {
        return books;
    }

    public List<Book> searchByAuthor(String author) {
        List<Book> booksAuthor = new ArrayList<>();
        for (Book book : books) {
            if (book.getAuthor().equalsIgnoreCase(author)) {
                booksAuthor.add(book);
            }
        }
        return booksAuthor;
    }

    public List<Book> searchByTitle(String title) {
        List<Book> booksTitle = new ArrayList<>();
        for (Book book : books) {
            if (book.getTitle().equalsIgnoreCase(title)) {
                booksTitle.add(book);
            }
        }
        return booksTitle;
    }

    public void delete(String code) {
        for (int i = 0; i < books.size(); i++) {
            if (books.get(i).getCode().equals(code)) {
                books.remove(i);
                JOptionPane.showMessageDialog(null, "Livro excluído com sucesso!");
            }
        }
    }
}
